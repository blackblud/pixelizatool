/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useRef } from "react";
import Button from "@mui/material/Button";
import AttachFileIcon from "@mui/icons-material/AttachFile";
import Divider from "@mui/material/Divider";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormatColorFillIcon from "@mui/icons-material/FormatColorFill";
import AddCircleOutlineIcon from "@mui/icons-material/AddCircleOutline";
import LayersClearIcon from "@mui/icons-material/LayersClear";
import FormatClearIcon from "@mui/icons-material/FormatClear";
import DownloadIcon from "@mui/icons-material/Download";
import Skeleton from "@mui/material/Skeleton";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";

import imageAlgorithm from "./tools/imageAlgorithm";
import downloadCanvas from "./tools/downloadCanvas";
import listToMatrix from "./tools/listToMatrix";
import fileToDataUri from "./tools/fileToDataUri";
import alertDelay from "./tools/alertDelay";
import timeout from "./tools/timeout";

export default function App() {
  const [square, setSquare] = useState("");
  const [rectangle, setRectangle] = useState("");
  const [color, setColor] = useState("RGB");

  const [canvasBeforeLoaded, setCanvasBeforeLoaded] = useState(false);
  const [contextBefore, setContextBefore] = useState();
  const [canvasAfterLoaded, setCanvasAfterLoaded] = useState(false);
  const [imageSize, setImageSize] = useState({});

  const [alertError, setAlertError] = useState(false);
  const [alertInfo, setAlertInfo] = useState(false);
  const [alertSuccess, setAlertSuccess] = useState(false);

  const canvasBeforeRef = useRef(null);
  const canvasAfterRef = useRef(null);
  const inputFileRef = useRef(null);

  useEffect(() => {
    inputFileRef.current.addEventListener("change", async (e) => {
      setCanvasBeforeLoaded(true);
      setCanvasAfterLoaded(true);

      const file = inputFileRef.current.files[0];

      const image = document.createElement("img");
      image.src = await fileToDataUri(file);

      await timeout(500);

      const canvas = canvasBeforeRef.current;
      const context = canvas.getContext("2d");

      const width = 800;
      const height = 400;

      canvas.width = width;
      canvas.height = height;

      context.drawImage(image, 0, 0, width, height);

      setContextBefore(context);
      alertDelay(setAlertInfo);

      const ctxAfter = canvasAfterRef.current.getContext("2d");
      ctxAfter.clearRect(0, 0, width, height);
    });
  }, []);

  const modeFillHandler = (figureSize) => {
    // 1. Перевірка на помилки
    if (!canvasBeforeLoaded) return alertDelay(setAlertError);

    // 2. Налаштування канвасу
    const canvas = canvasAfterRef.current;
    const context = canvas.getContext("2d");

    // 3. Розбиття масиву на матрицю
    const matrix = listToMatrix(
      contextBefore.getImageData(0, 0, 800, 400).data,
      3200
    );

    // 4. Структура вибору алгоритму
    const [result, width, height] = imageAlgorithm(
      figureSize,
      color,
      matrix,
      setAlertError
    );

    // 5. Формування готового зображення
    const finalImage = new ImageData(
      new Uint8ClampedArray(result.flat()),
      width,
      height
    );

    // // 6. Заповнення в CanvasAfter
    canvas.width = width;
    canvas.height = height;

    context.putImageData(finalImage, 0, 0);
    alertDelay(setAlertSuccess);
    setImageSize([800, 400, false]);
  };

  const modePixelHandler = (figureSize) => {
    // 1. Перевірка на помилки
    if (!canvasBeforeLoaded) return alertDelay(setAlertError);

    // 2. Налаштування канвасу
    const canvas = canvasAfterRef.current;
    const context = canvas.getContext("2d");

    // 3. Розбиття масиву на матрицю
    const matrix = listToMatrix(
      contextBefore.getImageData(0, 0, 800, 400).data,
      3200
    );

    // 4. Структура вибору алгоритму
    const [result, width, height] = imageAlgorithm(
      figureSize,
      color,
      matrix,
      setAlertError
    );

    // 5. Формування готового зображення
    const finalImage = new ImageData(
      new Uint8ClampedArray(result.flat()),
      width,
      height
    );

    // // 6. Заповнення в CanvasAfter
    canvas.width = 800;
    canvas.height = 400;

    context.putImageData(finalImage, (800 - width) / 2, (400 - height) / 2);
    // context.putImageData(finalImage, 0, 0);
    alertDelay(setAlertSuccess);
    setImageSize([width, height, true]);
  };

  return (
    <>
      <div className="app-block">
        <div className="app-panel">
          <p className="logo">Pixelizatool</p>

          <Divider sx={{ width: "100%", margin: "0 0 5px 0" }} />

          <input
            accept="image/*"
            style={{ display: "none" }}
            id="upload"
            type="file"
            ref={inputFileRef}
          />
          <label htmlFor="upload" className="label-import">
            <Button
              variant="contained"
              component="span"
              startIcon={<AttachFileIcon />}
              fullWidth
            >
              Upload Image
            </Button>
          </label>

          <Divider sx={{ width: "100%", margin: "10px 0" }} />

          <FormControl fullWidth>
            <InputLabel size="small">Square</InputLabel>
            <Select
              size="small"
              value={square}
              label="Square"
              onChange={(e) => {
                setSquare(e.target.value);
                setRectangle("");
              }}
            >
              <MenuItem value={"2x2"}>Square 2×2</MenuItem>
              <MenuItem value={"3x3"}>Square 3×3</MenuItem>
              <MenuItem value={"4x4"}>Square 4×4</MenuItem>
            </Select>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel size="small">Rectangle</InputLabel>
            <Select
              size="small"
              value={rectangle}
              label="Rectangle"
              onChange={(e) => {
                setSquare("");
                setRectangle(e.target.value);
              }}
            >
              <MenuItem value={"1x2"}>Rectangle 1×2</MenuItem>
              <MenuItem value={"2x4"}>Rectangle 2×4</MenuItem>
              <MenuItem value={"3x6"}>Rectangle 3×6</MenuItem>
            </Select>
          </FormControl>

          <Divider sx={{ width: "100%", margin: "10px 0" }} />

          <div className="radio-blocks">
            <RadioGroup
              aria-labelledby="demo-radio-buttons-group-label"
              value={color ? color : "RGB"}
              onChange={(e) => setColor(e.target.value)}
              name="radio-buttons-group"
            >
              <div className="radio-block">
                <FormControlLabel value="RGB" control={<Radio />} label="RGB" />
                <FormControlLabel value="HSV" control={<Radio />} label="HSV" />
              </div>
              <div className="radio-block">
                <FormControlLabel value="HSL" control={<Radio />} label="HSL" />
                <FormControlLabel
                  title={"Only for Square 3x3"}
                  value="Pixel"
                  control={<Radio />}
                  label="Pixel"
                />
              </div>
            </RadioGroup>
          </div>

          <Divider sx={{ width: "100%", margin: "10px 0" }} />

          <Button
            variant="contained"
            startIcon={<FormatColorFillIcon />}
            fullWidth
            onClick={() => {
              if (square !== "") {
                modeFillHandler(square);
              } else if (rectangle !== "") {
                modeFillHandler(rectangle);
              } else {
                alertDelay(setAlertError);
              }
            }}
          >
            Fill Selected Area
          </Button>
          <Button
            variant="contained"
            startIcon={<AddCircleOutlineIcon />}
            fullWidth
            onClick={() => {
              if (square !== "") {
                modePixelHandler(square);
              } else if (rectangle !== "") {
                modePixelHandler(rectangle);
              } else {
                alertDelay(setAlertError);
              }
            }}
          >
            Create a New Pixel
          </Button>
          <Divider sx={{ width: "100%", margin: "10px 0" }} />
          <Button
            variant="outlined"
            startIcon={<LayersClearIcon />}
            fullWidth
            onClick={() => {
              setCanvasBeforeLoaded(false);
              setCanvasAfterLoaded(false);
            }}
          >
            Clear Uploaded Image
          </Button>
          <Button
            variant="outlined"
            startIcon={<FormatClearIcon />}
            fullWidth
            onClick={() => {
              setSquare("");
              setRectangle("");
              setColor("RGB");
            }}
          >
            Reset Control Panel
          </Button>
          <Button
            variant="outlined"
            startIcon={<DownloadIcon />}
            fullWidth
            onClick={() => {
              if (!downloadCanvas(canvasAfterRef, imageSize)) {
                alertDelay(setAlertError);
              }
            }}
          >
            Download Image as PNG
          </Button>
        </div>

        <Divider orientation="vertical" />

        <div className="app-images">
          <div className="app-image">
            {canvasBeforeLoaded ? (
              <canvas ref={canvasBeforeRef}></canvas>
            ) : (
              <Skeleton
                variant="rounded"
                width={100 + "%"}
                height={100 + "%"}
              />
            )}
          </div>

          <Divider sx={{ width: "100%", margin: "4px 0" }} />

          <div className="app-image">
            {canvasAfterLoaded ? (
              <canvas ref={canvasAfterRef}></canvas>
            ) : (
              <Skeleton
                variant="rounded"
                width={100 + "%"}
                height={100 + "%"}
              />
            )}
          </div>
        </div>
      </div>

      {alertError && (
        <div className="alert">
          <Alert severity="error" variant="filled">
            <AlertTitle>Error</AlertTitle>
            Please <strong>upload an image</strong> and{" "}
            <strong>select a shape size</strong>. <br /> Also remember:{" "}
            <strong>Pixel mode</strong> is only for a 3x3 square.
          </Alert>
        </div>
      )}
      {alertInfo && (
        <div className="alert">
          <Alert severity="info" variant="filled">
            <AlertTitle>Info</AlertTitle>
            Your image has been <strong>successfully uploaded</strong> and is
            ready to processing
          </Alert>
        </div>
      )}
      {alertSuccess && (
        <div className="alert">
          <Alert severity="success" variant="filled">
            <AlertTitle>Success</AlertTitle>
            {square !== "" ? (
              <>
                Your image has been processed according to the parameters -{" "}
                [FigureSize: <strong>{square}</strong>, ColorMode:{" "}
                <strong>{color}</strong>]
              </>
            ) : rectangle !== "" ? (
              <>
                Your image has been processed according to the parameters -{" "}
                [FigureSize: <strong>{square}</strong>, ColorMode:{" "}
                <strong>{color}</strong>]
              </>
            ) : (
              "Everything is Fine."
            )}
          </Alert>
        </div>
      )}
    </>
  );
}
