export default function alertDelay(setter) {
  setter(true);
  setTimeout(() => {
    setter(false);
  }, 3500);
}
