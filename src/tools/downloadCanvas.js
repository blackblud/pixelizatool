export default function downloadCanvas(canvas, [width, height, mode]) {
  if (canvas.current) {
    if (mode) {
      let hidden_canv = document.createElement("canvas");
      hidden_canv.style.display = "none";
      document.body.appendChild(hidden_canv);
      hidden_canv.width = 800;
      hidden_canv.height = 400;

      let hidden_ctx = hidden_canv.getContext("2d");
      hidden_ctx.drawImage(
        canvas.current,
        (800 - width) / 2 + 1, //Start Clipping
        (400 - height) / 2 + 1, //Start Clipping
        width - 2, //Clipping Width
        height - 2, //Clipping Height
        0, //Place X
        0, //Place Y
        hidden_canv.width, //Place Width
        hidden_canv.height //Place Height
      );

      let anchor = document.createElement("a");
      anchor.download = "PixelizatedCanvas.png";
      anchor.href = hidden_canv.toDataURL("image/png");
      anchor.click();
      anchor.remove();
      anchor.innerHTML = "Download";
      document.body.appendChild(anchor);
    } else {
      let anchor = document.createElement("a");
      anchor.download = "PixelizatedCanvas.png";
      anchor.href = canvas.current.toDataURL("image/png");
      anchor.click();
      anchor.remove();
      anchor.innerHTML = "Download";
      document.body.appendChild(anchor);
    }
  }

  return false;
}
