import square2x2RGB from "../utils/square2x2RGB";
import square2x2HSV from "../utils/square2x2HSV";
import square2x2HSL from "../utils/square2x2HSL";
import square3x3RGB from "../utils/square3x3RGB";
import square3x3HSV from "../utils/square3x3HSV";
import square3x3HSL from "../utils/square3x3HSL";
import square3x3Pixel from "../utils/square3x3Pixel";
import square4x4RGB from "../utils/square4x4RGB";
import square4x4HSV from "../utils/square4x4HSV";
import square4x4HSL from "../utils/square4x4HSL";
import rectangle1x2RGB from "../utils/rectangle1x2RGB";
import rectangle1x2HSV from "../utils/rectangle1x2HSV";
import rectangle1x2HSL from "../utils/rectangle1x2HSL";
import rectangle2x4RGB from "../utils/rectangle2x4RGB";
import rectangle2x4HSV from "../utils/rectangle2x4HSV";
import rectangle2x4HSL from "../utils/rectangle2x4HSL";
import rectangle3x6RGB from "../utils/rectangle3x6RGB";
import rectangle3x6HSV from "../utils/rectangle3x6HSV";
import rectangle3x6HSL from "../utils/rectangle3x6HSL";

import alertDelay from "./alertDelay";

export default function imageAlgorithm(
  figureSize,
  color,
  matrix,
  setAlertError
) {
  let width = 0;
  let height = 0;
  let processedData = null;

  if (figureSize === "2x2") {
    width = 400;
    height = 200;

    if (color === "RGB") {
      console.log("2x2 - RGB");

      processedData = square2x2RGB(matrix);
    } else if (color === "HSV") {
      console.log("2x2 - HSV");

      processedData = square2x2HSV(matrix);
    } else if (color === "HSL") {
      console.log("2x2 - HSL");

      processedData = square2x2HSL(matrix);
    } else alertDelay(setAlertError);
  } else if (figureSize === "3x3") {
    width = 266; // 798
    height = 133; // 399

    if (color === "RGB") {
      console.log("3x3 - RGB");

      processedData = square3x3RGB(matrix);
    } else if (color === "HSV") {
      console.log("3x3 - HSV");

      processedData = square3x3HSV(matrix);
    } else if (color === "HSL") {
      console.log("3x3 - HSL");

      processedData = square3x3HSL(matrix);
    } else if (color === "Pixel") {
      console.log("3x3 - Pixel");

      processedData = square3x3Pixel(matrix);
    } else alertDelay(setAlertError);
  } else if (figureSize === "4x4") {
    width = 200;
    height = 100;

    if (color === "RGB") {
      console.log("4x4 - RGB");

      processedData = square4x4RGB(matrix);
    } else if (color === "HSV") {
      console.log("4x4 - HSV");

      processedData = square4x4HSV(matrix);
    } else if (color === "HSL") {
      console.log("4x4 - HSL");

      processedData = square4x4HSL(matrix);
    } else alertDelay(setAlertError);
  } else if (figureSize === "1x2") {
    width = 800;
    height = 200;

    if (color === "RGB") {
      console.log("1x2 - RGB");

      processedData = rectangle1x2RGB(matrix);
    } else if (color === "HSV") {
      console.log("1x2 - HSV");

      processedData = rectangle1x2HSV(matrix);
    } else if (color === "HSL") {
      console.log("1x2 - HSL");

      processedData = rectangle1x2HSL(matrix);
    } else alertDelay(setAlertError);
  } else if (figureSize === "2x4") {
    width = 400;
    height = 100;

    if (color === "RGB") {
      console.log("2x4 - RGB");

      processedData = rectangle2x4RGB(matrix);
    } else if (color === "HSV") {
      console.log("2x4 - HSV");

      processedData = rectangle2x4HSV(matrix);
    } else if (color === "HSL") {
      console.log("2x4 - HSL");

      processedData = rectangle2x4HSL(matrix);
    } else alertDelay(setAlertError);
  } else if (figureSize === "3x6") {
    width = 266; // 798
    height = 66; // 396

    if (color === "RGB") {
      console.log("3x6 - RGB");

      processedData = rectangle3x6RGB(matrix);
    } else if (color === "HSV") {
      console.log("3x6 - HSV");

      processedData = rectangle3x6HSV(matrix);
    } else if (color === "HSL") {
      console.log("3x6 - HSL");

      processedData = rectangle3x6HSL(matrix);
    } else alertDelay(setAlertError);
  } else alertDelay(setAlertError);

  return [processedData, width, height];
}
