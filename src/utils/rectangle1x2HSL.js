import Converter from "color-convert";

export default function rectangle1x2HSL(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 2) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200

    for (let j = 0; j < arr1.length; j += 4) {
      const px1HSL = Converter.rgb.hsl(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSL = Converter.rgb.hsl(arr2[j], arr2[j + 1], arr2[j + 2]);

      const H = Math.round((px1HSL[0] + px2HSL[0]) / 2);
      const S = Math.round((px1HSL[1] + px2HSL[1]) / 2);
      const L = Math.round((px1HSL[2] + px2HSL[2]) / 2);

      const pxRGB = Converter.hsl.rgb(H, S, L);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
