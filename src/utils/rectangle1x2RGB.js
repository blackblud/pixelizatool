export default function rectangle1x2RGB(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 2) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200

    for (let j = 0; j < arr1.length; j += 4) {
      const px1 = [arr1[j], arr1[j + 1], arr1[j + 2], arr1[j + 3]];
      const px2 = [arr2[j], arr2[j + 1], arr2[j + 2], arr2[j + 3]];

      const R = Math.round((px1[0] + px2[0]) / 2);
      const G = Math.round((px1[1] + px2[1]) / 2);
      const B = Math.round((px1[2] + px2[2]) / 2);
      const A = Math.round((px1[3] + px2[3]) / 2);

      const newPixel = [R, G, B, A];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
