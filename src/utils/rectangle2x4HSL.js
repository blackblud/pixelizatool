import Converter from "color-convert";

export default function rectangle2x4HSL(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 4) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200
    const arr3 = matrix[i + 2]; //3200
    const arr4 = matrix[i + 3]; //3200

    for (let j = 0; j < arr1.length; j += 8) {
      const px1HSL = Converter.rgb.hsl(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSL = Converter.rgb.hsl(arr1[j + 4], arr1[j + 5], arr1[j + 6]);
      const px3HSL = Converter.rgb.hsl(arr2[j], arr2[j + 1], arr2[j + 2]);
      const px4HSL = Converter.rgb.hsl(arr2[j + 4], arr2[j + 5], arr2[j + 6]);

      const px5HSL = Converter.rgb.hsl(arr3[j], arr3[j + 1], arr3[j + 2]);
      const px6HSL = Converter.rgb.hsl(arr3[j + 4], arr3[j + 5], arr3[j + 6]);
      const px7HSL = Converter.rgb.hsl(arr4[j], arr4[j + 1], arr4[j + 2]);
      const px8HSL = Converter.rgb.hsl(arr4[j + 4], arr4[j + 5], arr4[j + 6]);

      const H = Math.round(
        (px1HSL[0] +
          px2HSL[0] +
          px3HSL[0] +
          px4HSL[0] +
          px5HSL[0] +
          px6HSL[0] +
          px7HSL[0] +
          px8HSL[0]) /
          8
      );
      const S = Math.round(
        (px1HSL[1] +
          px2HSL[1] +
          px3HSL[1] +
          px4HSL[1] +
          px5HSL[1] +
          px6HSL[1] +
          px7HSL[1] +
          px8HSL[1]) /
          8
      );

      const L = Math.round(
        (px1HSL[2] +
          px2HSL[2] +
          px3HSL[2] +
          px4HSL[2] +
          px5HSL[2] +
          px6HSL[2] +
          px7HSL[2] +
          px8HSL[2]) /
          8
      );

      const pxRGB = Converter.hsl.rgb(H, S, L);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
