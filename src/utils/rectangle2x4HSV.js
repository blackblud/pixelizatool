import Converter from "color-convert";

export default function rectangle2x4HSV(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 4) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200
    const arr3 = matrix[i + 2]; //3200
    const arr4 = matrix[i + 3]; //3200

    for (let j = 0; j < arr1.length; j += 8) {
      const px1HSV = Converter.rgb.hsv(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSV = Converter.rgb.hsv(arr1[j + 4], arr1[j + 5], arr1[j + 6]);
      const px3HSV = Converter.rgb.hsv(arr2[j], arr2[j + 1], arr2[j + 2]);
      const px4HSV = Converter.rgb.hsv(arr2[j + 4], arr2[j + 5], arr2[j + 6]);

      const px5HSV = Converter.rgb.hsv(arr3[j], arr3[j + 1], arr3[j + 2]);
      const px6HSV = Converter.rgb.hsv(arr3[j + 4], arr3[j + 5], arr3[j + 6]);
      const px7HSV = Converter.rgb.hsv(arr4[j], arr4[j + 1], arr4[j + 2]);
      const px8HSV = Converter.rgb.hsv(arr4[j + 4], arr4[j + 5], arr4[j + 6]);

      const H = Math.round(
        (px1HSV[0] +
          px2HSV[0] +
          px3HSV[0] +
          px4HSV[0] +
          px5HSV[0] +
          px6HSV[0] +
          px7HSV[0] +
          px8HSV[0]) /
          8
      );
      const S = Math.round(
        (px1HSV[1] +
          px2HSV[1] +
          px3HSV[1] +
          px4HSV[1] +
          px5HSV[1] +
          px6HSV[1] +
          px7HSV[1] +
          px8HSV[1]) /
          8
      );

      const V = Math.round(
        (px1HSV[2] +
          px2HSV[2] +
          px3HSV[2] +
          px4HSV[2] +
          px5HSV[2] +
          px6HSV[2] +
          px7HSV[2] +
          px8HSV[2]) /
          8
      );

      const pxRGB = Converter.hsv.rgb(H, S, V);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
