export default function rectangle2x4RGB(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 4) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200
    const arr3 = matrix[i + 2]; //3200
    const arr4 = matrix[i + 3]; //3200

    for (let j = 0; j < arr1.length; j += 8) {
      const px1 = [arr1[j], arr1[j + 1], arr1[j + 2], arr1[j + 3]];
      const px2 = [arr1[j + 4], arr1[j + 5], arr1[j + 6], arr1[j + 7]];
      const px3 = [arr2[j], arr2[j + 1], arr2[j + 2], arr2[j + 3]];
      const px4 = [arr2[j + 4], arr2[j + 5], arr2[j + 6], arr2[j + 7]];

      const px5 = [arr3[j], arr3[j + 1], arr3[j + 2], arr3[j + 3]];
      const px6 = [arr3[j + 4], arr3[j + 5], arr3[j + 6], arr3[j + 7]];
      const px7 = [arr4[j], arr4[j + 1], arr4[j + 2], arr4[j + 3]];
      const px8 = [arr4[j + 4], arr4[j + 5], arr4[j + 6], arr4[j + 7]];

      const R = Math.round(
        (px1[0] +
          px2[0] +
          px3[0] +
          px4[0] +
          px5[0] +
          px6[0] +
          px7[0] +
          px8[0]) /
          8
      );
      const G = Math.round(
        (px1[1] +
          px2[1] +
          px3[1] +
          px4[1] +
          px5[1] +
          px6[1] +
          px7[1] +
          px8[1]) /
          8
      );
      const B = Math.round(
        (px1[2] +
          px2[2] +
          px3[2] +
          px4[2] +
          px5[2] +
          px6[2] +
          px7[2] +
          px8[2]) /
          8
      );
      const A = Math.round(
        (px1[3] +
          px2[3] +
          px3[3] +
          px4[3] +
          px5[3] +
          px6[3] +
          px7[3] +
          px8[3]) /
          8
      );

      const newPixel = [R, G, B, A];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
