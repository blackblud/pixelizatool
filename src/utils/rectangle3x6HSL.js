import Converter from "color-convert";

export default function rectangle3x6HSL(matrix) {
  const arrayPixels = [];
  matrix = matrix.slice(0, 396);

  for (let i = 0; i < matrix.length - 1; i += 6) {
    const arr1 = matrix[i].slice(0, 3191); //3200
    const arr2 = matrix[i + 1].slice(0, 3191); //3200
    const arr3 = matrix[i + 2].slice(0, 3191); //3200
    const arr4 = matrix[i + 3].slice(0, 3191); //3200
    const arr5 = matrix[i + 4].slice(0, 3191); //3200
    const arr6 = matrix[i + 5].slice(0, 3191); //3200

    for (let j = 0; j < arr1.length; j += 12) {
      const px1HSL = Converter.rgb.hsl(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSL = Converter.rgb.hsl(arr1[j + 4], arr1[j + 5], arr1[j + 6]);
      const px3HSL = Converter.rgb.hsl(arr1[j + 8], arr1[j + 9], arr1[j + 10]);

      const px4HSL = Converter.rgb.hsl(arr2[j], arr2[j + 1], arr2[j + 2]);
      const px5HSL = Converter.rgb.hsl(arr2[j + 4], arr2[j + 5], arr2[j + 6]);
      const px6HSL = Converter.rgb.hsl(arr2[j + 8], arr2[j + 9], arr2[j + 10]);

      const px7HSL = Converter.rgb.hsl(arr3[j], arr3[j + 1], arr3[j + 2]);
      const px8HSL = Converter.rgb.hsl(arr3[j + 4], arr3[j + 5], arr3[j + 6]);
      const px9HSL = Converter.rgb.hsl(arr3[j + 8], arr3[j + 9], arr3[j + 10]);

      const px10HSL = Converter.rgb.hsl(arr4[j], arr4[j + 1], arr4[j + 2]);
      const px11HSL = Converter.rgb.hsl(arr4[j + 4], arr4[j + 5], arr4[j + 6]);
      const px12HSL = Converter.rgb.hsl(arr4[j + 8], arr4[j + 9], arr4[j + 10]);

      const px13HSL = Converter.rgb.hsl(arr5[j], arr5[j + 1], arr5[j + 2]);
      const px14HSL = Converter.rgb.hsl(arr5[j + 4], arr5[j + 5], arr5[j + 6]);
      const px15HSL = Converter.rgb.hsl(arr5[j + 8], arr5[j + 9], arr5[j + 10]);

      const px16HSL = Converter.rgb.hsl(arr6[j], arr6[j + 1], arr6[j + 2]);
      const px17HSL = Converter.rgb.hsl(arr6[j + 4], arr6[j + 5], arr6[j + 6]);
      const px18HSL = Converter.rgb.hsl(arr6[j + 8], arr6[j + 9], arr6[j + 10]);

      const H = Math.round(
        (px1HSL[0] +
          px2HSL[0] +
          px3HSL[0] +
          px4HSL[0] +
          px5HSL[0] +
          px6HSL[0] +
          px7HSL[0] +
          px8HSL[0] +
          px9HSL[0] +
          px10HSL[0] +
          px11HSL[0] +
          px12HSL[0] +
          px13HSL[0] +
          px14HSL[0] +
          px15HSL[0] +
          px16HSL[0] +
          px17HSL[0] +
          px18HSL[0]) /
          18
      );

      const S = Math.round(
        (px1HSL[1] +
          px2HSL[1] +
          px3HSL[1] +
          px4HSL[1] +
          px5HSL[1] +
          px6HSL[1] +
          px7HSL[1] +
          px8HSL[1] +
          px9HSL[1] +
          px10HSL[1] +
          px11HSL[1] +
          px12HSL[1] +
          px13HSL[1] +
          px14HSL[1] +
          px15HSL[1] +
          px16HSL[1] +
          px17HSL[1] +
          px18HSL[1]) /
          18
      );
      const L = Math.round(
        (px1HSL[2] +
          px2HSL[2] +
          px3HSL[2] +
          px4HSL[2] +
          px5HSL[2] +
          px6HSL[2] +
          px7HSL[2] +
          px8HSL[2] +
          px9HSL[2] +
          px10HSL[2] +
          px11HSL[2] +
          px12HSL[2] +
          px13HSL[2] +
          px14HSL[2] +
          px15HSL[2] +
          px16HSL[2] +
          px17HSL[2] +
          px18HSL[2]) /
          18
      );

      const pxRGB = Converter.hsl.rgb(H, S, L);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
