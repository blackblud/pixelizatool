export default function rectangle3x6RGB(matrix) {
  const arrayPixels = [];
  matrix = matrix.slice(0, 396);

  for (let i = 0; i < matrix.length - 1; i += 6) {
    const arr1 = matrix[i].slice(0, 3191); //3200
    const arr2 = matrix[i + 1].slice(0, 3191); //3200
    const arr3 = matrix[i + 2].slice(0, 3191); //3200
    const arr4 = matrix[i + 3].slice(0, 3191); //3200
    const arr5 = matrix[i + 4].slice(0, 3191); //3200
    const arr6 = matrix[i + 5].slice(0, 3191); //3200

    for (let j = 0; j < arr1.length; j += 12) {
      const px1 = [arr1[j], arr1[j + 1], arr1[j + 2], arr1[j + 3]];
      const px2 = [arr1[j + 4], arr1[j + 5], arr1[j + 6], arr1[j + 7]];
      const px3 = [arr1[j + 8], arr1[j + 9], arr1[j + 10], arr1[j + 11]];

      const px4 = [arr2[j], arr2[j + 1], arr2[j + 2], arr2[j + 3]];
      const px5 = [arr2[j + 4], arr2[j + 5], arr2[j + 6], arr2[j + 7]];
      const px6 = [arr2[j + 8], arr2[j + 9], arr2[j + 10], arr2[j + 11]];

      const px7 = [arr3[j], arr3[j + 1], arr3[j + 2], arr3[j + 3]];
      const px8 = [arr3[j + 4], arr3[j + 5], arr3[j + 6], arr3[j + 7]];
      const px9 = [arr3[j + 8], arr3[j + 9], arr3[j + 10], arr3[j + 11]];

      const px10 = [arr4[j], arr4[j + 1], arr4[j + 2], arr4[j + 3]];
      const px11 = [arr4[j + 4], arr4[j + 5], arr4[j + 6], arr4[j + 7]];
      const px12 = [arr4[j + 8], arr4[j + 9], arr4[j + 10], arr4[j + 11]];

      const px13 = [arr5[j], arr5[j + 1], arr5[j + 2], arr5[j + 3]];
      const px14 = [arr5[j + 4], arr5[j + 5], arr5[j + 6], arr5[j + 7]];
      const px15 = [arr5[j + 8], arr5[j + 9], arr5[j + 10], arr5[j + 11]];

      const px16 = [arr6[j], arr6[j + 1], arr6[j + 2], arr6[j + 3]];
      const px17 = [arr6[j + 4], arr6[j + 5], arr6[j + 6], arr6[j + 7]];
      const px18 = [arr6[j + 8], arr6[j + 9], arr6[j + 10], arr6[j + 11]];

      const R = Math.round(
        (px1[0] +
          px2[0] +
          px3[0] +
          px4[0] +
          px5[0] +
          px6[0] +
          px7[0] +
          px8[0] +
          px9[0] +
          px10[0] +
          px11[0] +
          px12[0] +
          px13[0] +
          px14[0] +
          px15[0] +
          px16[0] +
          px17[0] +
          px18[0]) /
          18
      );
      const G = Math.round(
        (px1[1] +
          px2[1] +
          px3[1] +
          px4[1] +
          px5[1] +
          px6[1] +
          px7[1] +
          px8[1] +
          px9[1] +
          px10[1] +
          px11[1] +
          px12[1] +
          px13[1] +
          px14[1] +
          px15[1] +
          px16[1] +
          px17[1] +
          px18[1]) /
          18
      );
      const B = Math.round(
        (px1[2] +
          px2[2] +
          px3[2] +
          px4[2] +
          px5[2] +
          px6[2] +
          px7[2] +
          px8[2] +
          px9[2] +
          px10[2] +
          px11[2] +
          px12[2] +
          px13[2] +
          px14[2] +
          px15[2] +
          px16[2] +
          px17[2] +
          px18[2]) /
          18
      );
      const A = Math.round(
        (px1[3] +
          px2[3] +
          px3[3] +
          px4[3] +
          px5[3] +
          px6[3] +
          px7[3] +
          px8[3] +
          px9[3] +
          px10[3] +
          px11[3] +
          px12[3] +
          px13[3] +
          px14[3] +
          px15[3] +
          px16[3] +
          px17[3] +
          px18[3]) /
          18
      );

      const newPixel = [R, G, B, A];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
