import Converter from "color-convert";

export default function square2x2HSL(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 2) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200

    for (let j = 0; j < arr1.length; j += 8) {
      const px1HSL = Converter.rgb.hsl(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSL = Converter.rgb.hsl(arr1[j + 4], arr1[j + 5], arr1[j + 6]);
      const px3HSL = Converter.rgb.hsl(arr2[j], arr2[j + 1], arr2[j + 2]);
      const px4HSL = Converter.rgb.hsl(arr2[j + 4], arr2[j + 5], arr2[j + 6]);

      const H = Math.round((px1HSL[0] + px2HSL[0] + px3HSL[0] + px4HSL[0]) / 4);
      const S = Math.round((px1HSL[1] + px2HSL[1] + px3HSL[1] + px4HSL[1]) / 4);
      const L = Math.round((px1HSL[2] + px2HSL[2] + px3HSL[2] + px4HSL[2]) / 4);

      const pxRGB = Converter.hsl.rgb(H, S, L);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
