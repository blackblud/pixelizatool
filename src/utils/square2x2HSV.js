import Converter from "color-convert";

export default function square2x2HSV(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 2) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200

    for (let j = 0; j < arr1.length; j += 8) {
      const px1HSV = Converter.rgb.hsv(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSV = Converter.rgb.hsv(arr1[j + 4], arr1[j + 5], arr1[j + 6]);
      const px3HSV = Converter.rgb.hsv(arr2[j], arr2[j + 1], arr2[j + 2]);
      const px4HSV = Converter.rgb.hsv(arr2[j + 4], arr2[j + 5], arr2[j + 6]);

      const H = Math.round((px1HSV[0] + px2HSV[0] + px3HSV[0] + px4HSV[0]) / 4);
      const S = Math.round((px1HSV[1] + px2HSV[1] + px3HSV[1] + px4HSV[1]) / 4);
      const V = Math.round((px1HSV[2] + px2HSV[2] + px3HSV[2] + px4HSV[2]) / 4);

      const pxRGB = Converter.hsv.rgb(H, S, V);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
