export default function square2x2RGB(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 2) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200

    for (let j = 0; j < arr1.length; j += 8) {
      const px1 = [arr1[j], arr1[j + 1], arr1[j + 2], arr1[j + 3]];
      const px2 = [arr1[j + 4], arr1[j + 5], arr1[j + 6], arr1[j + 7]];
      const px3 = [arr2[j], arr2[j + 1], arr2[j + 2], arr2[j + 3]];
      const px4 = [arr2[j + 4], arr2[j + 5], arr2[j + 6], arr2[j + 7]];

      const R = Math.round((px1[0] + px2[0] + px3[0] + px4[0]) / 4);
      const G = Math.round((px1[1] + px2[1] + px3[1] + px4[1]) / 4);
      const B = Math.round((px1[2] + px2[2] + px3[2] + px4[2]) / 4);
      const A = Math.round((px1[3] + px2[3] + px3[3] + px4[3]) / 4);

      const newPixel = [R, G, B, A];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
