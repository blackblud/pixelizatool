export default function square3x3Pixel(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 3) {
    const arr2 = matrix[i + 1].slice(0, 3191); //3200

    for (let j = 0; j < arr2.length; j += 12) {
      const px1 = [arr2[j + 4], arr2[j + 5], arr2[j + 6], arr2[j + 7]];
      const newPixel = [px1[0], px1[1], px1[2], px1[3]];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
