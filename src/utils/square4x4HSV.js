import Converter from "color-convert";

export default function square4x4HSL(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 4) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200
    const arr3 = matrix[i + 2]; //3200
    const arr4 = matrix[i + 3]; //3200

    for (let j = 0; j < arr1.length; j += 16) {
      const px1HSV = Converter.rgb.hsv(arr1[j], arr1[j + 1], arr1[j + 2]);
      const px2HSV = Converter.rgb.hsv(arr1[j + 4], arr1[j + 5], arr1[j + 6]);
      const px3HSV = Converter.rgb.hsv(arr1[j + 8], arr1[j + 9], arr1[j + 10]);
      const px4HSV = Converter.rgb.hsv(
        arr1[j + 12],
        arr1[j + 13],
        arr1[j + 14]
      );

      const px5HSV = Converter.rgb.hsv(arr2[j], arr2[j + 1], arr2[j + 2]);
      const px6HSV = Converter.rgb.hsv(arr2[j + 4], arr2[j + 5], arr2[j + 6]);
      const px7HSV = Converter.rgb.hsv(arr2[j + 8], arr2[j + 9], arr2[j + 10]);
      const px8HSV = Converter.rgb.hsv(
        arr2[j + 12],
        arr2[j + 13],
        arr2[j + 14]
      );

      const px9HSV = Converter.rgb.hsv(arr3[j], arr3[j + 1], arr3[j + 2]);
      const px10HSV = Converter.rgb.hsv(arr3[j + 4], arr3[j + 5], arr3[j + 6]);
      const px11HSV = Converter.rgb.hsv(arr3[j + 8], arr3[j + 9], arr3[j + 10]);
      const px12HSV = Converter.rgb.hsv(
        arr3[j + 12],
        arr3[j + 13],
        arr3[j + 14]
      );

      const px13HSV = Converter.rgb.hsv(arr4[j], arr4[j + 1], arr4[j + 2]);
      const px14HSV = Converter.rgb.hsv(arr4[j + 4], arr4[j + 5], arr4[j + 6]);
      const px15HSV = Converter.rgb.hsv(arr4[j + 8], arr4[j + 9], arr4[j + 10]);
      const px16HSV = Converter.rgb.hsv(
        arr4[j + 12],
        arr4[j + 13],
        arr4[j + 14]
      );

      const H = Math.round(
        (px1HSV[0] +
          px2HSV[0] +
          px3HSV[0] +
          px4HSV[0] +
          px5HSV[0] +
          px6HSV[0] +
          px7HSV[0] +
          px8HSV[0] +
          px9HSV[0] +
          px10HSV[0] +
          px11HSV[0] +
          px12HSV[0] +
          px13HSV[0] +
          px14HSV[0] +
          px15HSV[0] +
          px16HSV[0]) /
          16
      );
      const S = Math.round(
        (px1HSV[1] +
          px2HSV[1] +
          px3HSV[1] +
          px4HSV[1] +
          px5HSV[1] +
          px6HSV[1] +
          px7HSV[1] +
          px8HSV[1] +
          px9HSV[1] +
          px10HSV[1] +
          px11HSV[1] +
          px12HSV[1] +
          px13HSV[1] +
          px14HSV[1] +
          px15HSV[1] +
          px16HSV[1]) /
          16
      );
      const V = Math.round(
        (px1HSV[2] +
          px2HSV[2] +
          px3HSV[2] +
          px4HSV[2] +
          px5HSV[2] +
          px6HSV[2] +
          px7HSV[2] +
          px8HSV[2] +
          px9HSV[2] +
          px10HSV[2] +
          px11HSV[2] +
          px12HSV[2] +
          px13HSV[2] +
          px14HSV[2] +
          px15HSV[2] +
          px16HSV[2]) /
          16
      );

      const pxRGB = Converter.hsv.rgb(H, S, V);

      const newPixel = [pxRGB[0], pxRGB[1], pxRGB[2], 255];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
