export default function square4x4RGB(matrix) {
  const arrayPixels = [];

  for (let i = 0; i < matrix.length - 1; i += 4) {
    const arr1 = matrix[i]; //3200
    const arr2 = matrix[i + 1]; //3200
    const arr3 = matrix[i + 2]; //3200
    const arr4 = matrix[i + 3]; //3200

    for (let j = 0; j < arr1.length; j += 16) {
      const px1 = [arr1[j], arr1[j + 1], arr1[j + 2], arr1[j + 3]];
      const px2 = [arr1[j + 4], arr1[j + 5], arr1[j + 6], arr1[j + 7]];
      const px3 = [arr1[j + 8], arr1[j + 9], arr1[j + 10], arr1[j + 11]];
      const px4 = [arr1[j + 12], arr1[j + 13], arr1[j + 14], arr1[j + 15]];

      const px5 = [arr2[j], arr2[j + 1], arr2[j + 2], arr2[j + 3]];
      const px6 = [arr2[j + 4], arr2[j + 5], arr2[j + 6], arr2[j + 7]];
      const px7 = [arr2[j + 8], arr2[j + 9], arr2[j + 10], arr2[j + 11]];
      const px8 = [arr2[j + 12], arr2[j + 13], arr2[j + 14], arr2[j + 15]];

      const px9 = [arr3[j], arr3[j + 1], arr3[j + 2], arr3[j + 3]];
      const px10 = [arr3[j + 4], arr3[j + 5], arr3[j + 6], arr3[j + 7]];
      const px11 = [arr3[j + 8], arr3[j + 9], arr3[j + 10], arr3[j + 11]];
      const px12 = [arr3[j + 12], arr3[j + 13], arr3[j + 14], arr3[j + 15]];

      const px13 = [arr4[j], arr4[j + 1], arr4[j + 2], arr4[j + 3]];
      const px14 = [arr4[j + 4], arr4[j + 5], arr4[j + 6], arr4[j + 7]];
      const px15 = [arr4[j + 8], arr4[j + 9], arr4[j + 10], arr4[j + 11]];
      const px16 = [arr4[j + 12], arr4[j + 13], arr4[j + 14], arr4[j + 15]];

      const R = Math.round(
        (px1[0] +
          px2[0] +
          px3[0] +
          px4[0] +
          px5[0] +
          px6[0] +
          px7[0] +
          px8[0] +
          px9[0] +
          px10[0] +
          px11[0] +
          px12[0] +
          px13[0] +
          px14[0] +
          px15[0] +
          px16[0]) /
          16
      );
      const G = Math.round(
        (px1[1] +
          px2[1] +
          px3[1] +
          px4[1] +
          px5[1] +
          px6[1] +
          px7[1] +
          px8[1] +
          px9[1] +
          px10[1] +
          px11[1] +
          px12[1] +
          px13[1] +
          px14[1] +
          px15[1] +
          px16[1]) /
          16
      );
      const B = Math.round(
        (px1[2] +
          px2[2] +
          px3[2] +
          px4[2] +
          px5[2] +
          px6[2] +
          px7[2] +
          px8[2] +
          px9[2] +
          px10[2] +
          px11[2] +
          px12[2] +
          px13[2] +
          px14[2] +
          px15[2] +
          px16[2]) /
          16
      );
      const A = Math.round(
        (px1[3] +
          px2[3] +
          px3[3] +
          px4[3] +
          px5[3] +
          px6[3] +
          px7[3] +
          px8[3] +
          px9[3] +
          px10[3] +
          px11[3] +
          px12[3] +
          px13[3] +
          px14[3] +
          px15[3] +
          px16[3]) /
          16
      );

      const newPixel = [R, G, B, A];
      arrayPixels.push(newPixel);
    }
  }

  return arrayPixels;
}
